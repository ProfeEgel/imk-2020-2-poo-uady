﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.Console;

namespace POOBasics
{
    /*
    struct Box
    {
        public int width;
        public int length;
        public int height;
    }
    */

    // Pilares de la POO
    // 1. Encapsulamiento / Ocultamiento de la información
    // 2. Herencia
    // 3. Polimorfismo

    // IMPORTANTE: es necesario que el programador de la clase mantenga
    //             válido el estado de un objeto en todo momento

    class Box
    {
        private int width;
        private int length;
        private int height;

        // propiedad
        public int Width
        {
            get => width;

            set
            {
                if (value <= 0)
                {
                    // generar error grave => excepción
                    throw new ArgumentOutOfRangeException();
                }

                width = value;
            }
        }

        public int Length
        {
            get => length;

            set
            {
                if (value <= 0)
                {
                    // generar error grave => excepción
                    throw new ArgumentOutOfRangeException();
                }

                length = value;
            }
        }

        public int Height
        {
            get => height;

            set
            {
                if (value <= 0)
                {
                    // generar error grave => excepción
                    throw new ArgumentOutOfRangeException();
                }

                height = value;
            }
        }

        // patrón de diseño get/set
        //public int GetWidth() => width;

        /*public void SetWidth(int width)
        {
            if (width <= 0)
            {
                // generar error grave => excepción
                throw new ArgumentOutOfRangeException();
            }

            this.width = width;
        }*/

        //public int CalculateVolume() => width * length * height;

        public int Volume
        {
            get => width * length * height;
        }

        // encadenamiento de constructores => constructor chaining

        public Box() : // constructor sin parámetros y vacío
            this(1) { }

        public Box(int size) :
            this(size, size, size) { }

        public Box(int width, int lenght, int height)
        {
            Width = width;
            Length = lenght;
            Height = height;
        }

        public Box(Box box) : // copy constructor
            this(box.width, box.length, box.height) { }
    }

    class Program
    {
        //static int CalculateVolume(Box box) =>
        //    box.width * box.length * box.height;

        static void Main(string[] args)
        {
            Box b = new Box(2, 1, 3);

            Box[] boxes = {
                new Box(),
                new Box(3),
                new Box(2, 1, 3),
                new Box(b) { Length = 5  }
            };

            for (int i = 0; i < boxes.Length; ++i)
            {
                WriteLine($"Volumen b{i+1} = {boxes[i].Volume}");
            }

            //WriteLine($"Volumen b1 = {Box.CalculateVolume(b1)}");
            //WriteLine($"Volumen b2 = {Box.CalculateVolume(b2)}");

            /*
            Box b1;
            b1.width = 2;
            b1.length = 1;
            b1.height = 3;

            Box b2;
            b2 = b1;
            b2.length = 5;
            //b2.width = 1;
            //b2.length = 3;
            //b2.height = 5;

            WriteLine($"Volumen b1 = {CalculateVolume(b1)}");
            WriteLine($"Volumen b2 = {CalculateVolume(b2)}");
            */

            //int volume = b1.width * b1.length * b1.height;
            //int volume = CalculateVolume(b1);
            //WriteLine($"Volumen b1 = {volume}");
        }
    }
}
