﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListsContainersDemo
{
    public partial class MainForm : Form
    {
        //private List<string> data = new List<string>();
        private List<Patient> data = new List<Patient>();

        public MainForm()
        {
            InitializeComponent();

            // ********************************************************
            // #1 - TIPOS BASICOS - ITEMS
            // SELECTED INDEX => OK
            // SELECTED ITEM => OK
            // SELECTED VALUE => N/A
            //listBox.Items.Add("C#");
            //listBox.Items.Add("C++");
            //listBox.Items.Add("Python");
            //listBox.Items.Add("Java");
            //listBox.Items.Add("Kotlin");
            //listBox.Items.Add("JavaScript");
            //listBox.Items.Add("D");
            //listBox.Items.Add("Dart");

            // ********************************************************
            // #2 - OBJETOS - ITEMS
            // SELECTED INDEX => OK
            // SELECTED ITEM => OK
            // SELECTED VALUE => N/A
            //listBox.Items.Add(new Patient("Juan", "Perez", 20));
            //listBox.Items.Add(new Patient("Erika", "Montalvo", 53));
            //listBox.Items.Add(new Patient("Manuel", "Verde", 22));
            //listBox.Items.Add(new Patient("Miguel", "Tellez", 38));
            //listBox.Items.Add(new Patient("Jessica", "Valdez", 19));
            //listBox.Items.Add(new Patient("Jorge", "Amendola", 47));
            //listBox.Items.Add(new Patient("Elizabeth", "Duarte", 36));

            //listBox.DisplayMember = "FullName";

            // ********************************************************
            // #3 - TIPOS BASICOS - DATASOURCE
            // SELECTED INDEX => OK
            // SELECTED ITEM => OK
            // SELECTED VALUE => OK
            //data.Add("C#");
            //data.Add("C++");
            //data.Add("Python");
            //data.Add("Java");
            //data.Add("Kotlin");
            //data.Add("JavaScript");
            //data.Add("D");
            //data.Add("Dart");

            //listBox.DataSource = data;

            // ********************************************************
            // #4 - OBJETOS - DATASOURCE
            // SELECTED INDEX => OK
            // SELECTED ITEM => OK
            // SELECTED VALUE => OK
            data.Add(new Patient("Juan", "Perez", 20));
            data.Add(new Patient("Erika", "Montalvo", 53));
            data.Add(new Patient("Manuel", "Verde", 22));
            data.Add(new Patient("Miguel", "Tellez", 38));
            data.Add(new Patient("Jessica", "Valdez", 19));
            data.Add(new Patient("Jorge", "Amendola", 47));
            data.Add(new Patient("Elizabeth", "Duarte", 36));

            listBox.DisplayMember = "FullName";
            listBox.ValueMember = "Age";
            listBox.DataSource = data;
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // #1, #2, #3
            txtIndex.Text = $"{listBox.SelectedIndex}";
            txtItem.Text = $"{listBox.SelectedItem}";
            txtValue.Text = $"{listBox.SelectedValue}";

            btnRemove.Enabled = listBox.SelectedIndex >= 0;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            // #1, #2
            //listBox.Items.RemoveAt(listBox.SelectedIndex);

            // #3, #4
            data.RemoveAt(listBox.SelectedIndex);
            // Refresh listbox
            listBox.DataSource = null;
            listBox.DataSource = data;
        }
    }

    class Patient
    {
        public string FirstName { get; }
        public string LastName { get; }
        public int Age { get; }

        public string FullName { get => $"{LastName}, {FirstName}"; }

        public Patient(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }

        public override string ToString()
        {
            return $"{LastName}, {FirstName} - {Age}";
        }
    }
}
