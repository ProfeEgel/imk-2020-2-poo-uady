﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentalCare
{
    class Time
    {
        public int Id { get; private set; }
        public string Description { get; private set; }

        public Time(int id, string description)
        {
            Id = id;
            Description = description;
        }

        public override string ToString() => Description;
    }
}
