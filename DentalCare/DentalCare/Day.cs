﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace DentalCare
{
    class Day
    {
        public int Id { get; private set; } // propiedad automática
        public string Name { get; private set; } // de solo lectura

        public Day(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override string ToString() => Name;
    }
}
