﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentalCare
{
    class PendingAppointment
    {
        public Patient Patient { get; private set; }
        public Day Day { get; private set; }
        public Time Time { get; private set; }

        public PendingAppointment(Patient patient, Day day, Time time)
        {
            Patient = patient;
            Day = day;
            Time = time;
        }
    }
}
