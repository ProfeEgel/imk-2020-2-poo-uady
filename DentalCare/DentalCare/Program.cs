﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.Console;

namespace DentalCare
{
    class Program
    {
        /*
        public static void Imprimelo(int elemento)
        {
            Write($"{elemento} ");
        }

        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            list.Add(5);
            list.Add(-6);
            list.Add(0);
            list.Add(1);
            list.Add(23);

            list.ForEach(e => Write($"{e} ")); // lambda function

            WriteLine();
            list.ForEach(Imprimelo);

            WriteLine();
            foreach (int elemento in list)
            {
                Write($"{elemento} ");
            }

            WriteLine();
            for (int i = 0; i < list.Count; ++i)
            {
                Write($"{list[i]} ");
            }
        }
        */

        enum Menu { SALIR, VOLVER=0,
                    CONSULTAS, CITAS,
                    CITAS_POR_PACIENTE=1, CITAS_POR_DIA,
                    AGENDAR=1, CANCELAR };

        static Agenda agenda = new Agenda();

        static void Main(string[] args)
        {
            Menu opcion = Menu.SALIR;
            do
            {
                Clear();
                WriteLine("***************************");
                WriteLine("***     DENTALCARE      ***");
                WriteLine("***************************");
                WriteLine();
                WriteLine("1 - Consultas");
                WriteLine("2 - Citas");
                WriteLine("0 - Salir");
                WriteLine();
                Write("Selecciona una opción: ");
                opcion = (Menu)Convert.ToInt32(ReadLine());

                switch (opcion)
                {
                    case Menu.CONSULTAS:
                        SubmenuConsultas();
                        break;

                    case Menu.CITAS:
                        SubmenuCitas();
                        break;

                    default:
                        WriteLine("\n¡OPCIÓN NO IMPLEMENTADA!");
                        ReadKey();
                        break;

                    case Menu.SALIR:
                        WriteLine("\n¡Gracias por utilizar el programa!");
                        break;
                }
            }
            while (opcion != Menu.SALIR);
        }

        static void SubmenuConsultas()
        {
            Menu opcion = Menu.VOLVER;
            do
            {
                Clear();
                WriteLine("***************************");
                WriteLine("***      CONSULTAS      ***");
                WriteLine("***************************");
                WriteLine();
                WriteLine("1 - Citas por paciente");
                WriteLine("2 - Citas por día");
                WriteLine("0 - Volver");
                WriteLine();
                Write("Selecciona una opción: ");
                opcion = (Menu)Convert.ToInt32(ReadLine());

                switch (opcion)
                {
                    case Menu.CITAS_POR_PACIENTE:
                        List<PendingAppointment> appointments = agenda.GetPendingAppointments();
                        appointments.Sort((c1, c2) =>
                            c1.Patient.LastName.CompareTo(c2.Patient.LastName));
                            //c1.Day.Id.CompareTo(c2.Day.Id));

                        // Comparison(a,b)
                        // a>b => +1
                        // a=b => 0
                        // a<b => -1

                        Clear();
                        WriteLine("\n***** CITAS PENDIENTES *****\n");
                        appointments.ForEach(a =>
                            WriteLine($"{a.Patient}. {a.Day} - {a.Time}"));
                            //WriteLine($"{a.Day} - {a.Time} {a.Patient}."));

                        ReadKey();
                        break;

                    case Menu.CITAS_POR_DIA:
                        List<Day> days = agenda.GetDays();
                        appointments = agenda.GetPendingAppointments();

                        Clear();
                        WriteLine("\n***** CITAS PENDIENTES *****\n");

                        days.ForEach(d =>
                        {
                            List<PendingAppointment> dayAppointments =
                                appointments.FindAll(a => a.Day.Id == d.Id);

                            if (dayAppointments.Count > 0)
                            {
                                WriteLine(d.Name);
                            }

                            dayAppointments.Sort((c1, c2) =>
                                c1.Time.Id.CompareTo(c2.Time.Id));

                            dayAppointments.ForEach(a =>
                                WriteLine($"    {a.Time} {a.Patient}."));
                        });

                        ReadKey();
                        break;

                    case Menu.VOLVER:
                    default:
                        break;
                }
            }
            while (opcion != Menu.VOLVER);
        }

        static void SubmenuCitas()
        {
            Menu opcion = Menu.VOLVER;
            do
            {
                Clear();
                WriteLine("***************************");
                WriteLine("***        CITAS        ***");
                WriteLine("***************************");
                WriteLine();
                WriteLine("1 - Agendar");
                WriteLine("2 - Cancelar");
                WriteLine("0 - Volver");
                WriteLine();
                Write("Selecciona una opción: ");
                opcion = (Menu)Convert.ToInt32(ReadLine());

                switch (opcion)
                {
                    case Menu.AGENDAR:
                        Clear();
                        WriteLine("\n***** AGENDAR CITA *****\n");
                        Write("Clave del paciente: ");
                        int patientId = Convert.ToInt32(ReadLine());

                        if (!agenda.HasPendingAppointment(patientId))
                        {
                            List<Schedule> availableSchedule = agenda.GetAvailableSchedule();
                            List<Day> days = agenda.GetDays();

                            WriteLine("\n** Días disponibles **");
                            for (int i = 0; i < days.Count; ++i)
                            {
                                if (availableSchedule.Exists(s => s.DayId == days[i].Id))
                                {
                                    WriteLine($"{i + 1} - {days[i].Name}");
                                }
                            }

                            Write("\nElige un día: ");
                            int dayIndex = Convert.ToInt32(ReadLine()) - 1;

                            List<Time> times = agenda.GetTimes();
                            List<Schedule> filteredSchedule = availableSchedule
                                                                .FindAll(s => s.DayId == days[dayIndex].Id);
                            filteredSchedule.Sort((s1, s2) => s1.TimeId.CompareTo(s2.TimeId));

                            WriteLine("\n** Horas disponibles **");
                            for (int i = 0; i < filteredSchedule.Count; ++i)
                            {
                                string description = 
                                    times.Find(t => t.Id == filteredSchedule[i].TimeId).Description;
                                
                                WriteLine($"{i + 1:d2} - {description}");
                            }

                            Write("\nElige una hora (0 para cancelar):  ");
                            int timeIndex = Convert.ToInt32(ReadLine()) - 1;

                            if (timeIndex >= 0)
                            {
                                // patientId
                                int dayId = days[dayIndex].Id;

                                // ESTA LINEA ERA EL BUG, EL INDEX ES CON RESPECTO AL filteredSchedule
                                // int timeId = times[timeIndex].Id;
                                int timeId = filteredSchedule[timeIndex].TimeId;

                                agenda.AddAppointment(patientId, dayId, timeId);

                                WriteLine("\n¡Cita agregada!");
                            }
                            else
                            {
                                WriteLine("\n¡Operación cancelada!");
                            }

                            ReadKey();
                        }
                        else
                        {
                            WriteLine("\n¡El paciente ya tiene una cita asignada!");
                            ReadKey();
                        }

                        break;

                    case Menu.CANCELAR:
                        Clear();
                        WriteLine("\n***** CANCELAR CITA *****\n");
                        Write("Clave del paciente: ");
                        patientId = Convert.ToInt32(ReadLine());

                        if (agenda.HasPendingAppointment(patientId))
                        {
                            Write("\nLa cita se eliminará a continuación ¿estás seguro?[s/n]: ");
                            if (ReadLine().ToLower()[0] == 's')
                            {
                                agenda.CancelAppointment(patientId);

                                WriteLine("\n¡La cita ha sido cancelada!");
                                ReadKey();
                            }
                            else
                            {
                                WriteLine("\n¡La operación fue cancelada!");
                                ReadKey();
                            }
                        }
                        else
                        {
                            WriteLine("\n¡El paciente no tiene una cita asignada!");
                            ReadKey();
                        }
                        break;

                    case Menu.VOLVER:
                    default:
                        break;
                }
            }
            while (opcion != Menu.VOLVER);
        }
    }
}

/*
Lunes
    09:00 a.m. Quintas, Xavier.
    09:30 a.m. Ros, Luis.
    10:00 a.m. Vargas, Antonio.
    11:00 a.m. Ariza, Adrian.
    11:30 a.m. Morillas, Sonia.
    12:00 p.m. Corona, Anna.
    12:30 p.m. Torrente, Oscar.
    01:00 p.m. Bermudez, Jesus.
    02:00 p.m. Garcia, Laia.
    01:30 p.m. Hurtado, Manuela.
    10:30 a.m. Huerta, Silvia.
Martes
    07:00 p.m. Pulido, Victor.
    06:30 p.m. Roman, Pablo.
    05:30 p.m. Bosch, Angeles.
    06:00 p.m. Sanchez, Marta.
    05:00 p.m. Rodriguez, Daniel.
    08:00 p.m. Maldonado, Jose.
*/