﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentalCare
{
    class Appointment
    {
        public int PatientId { get; private set; }
        public int DayId { get; private set; }
        public int TimeId { get; private set; }

        public Appointment(int patientId, int dayId, int timeId)
        {
            PatientId = patientId;
            DayId = dayId;
            TimeId = timeId;
        }
    }
}
