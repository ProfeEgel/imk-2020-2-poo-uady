﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace DentalCare
{
    class Agenda
    {
        private List<Day> days;
        private List<Time> times;
        private List<Schedule> schedules;
        private List<Patient> patients;
        private List<Appointment> appointments;
        
        public Agenda()
        {
            // "0|Lunes" => ["0", "Lunes"]
            days = EasyFile<Day>.LoadDataFromFile("days.txt",
                tokens => new Day(Convert.ToInt32(tokens[0]),
                                  tokens[1]));

            // "0|09:00 a.m." => ["0", "09:00 a.m."]
            times = EasyFile<Time>.LoadDataFromFile("time.txt",
                tokens => new Time(Convert.ToInt32(tokens[0]),
                                   tokens[1]));

            // "0|0" => ["0", "0"]
            schedules = EasyFile<Schedule>.LoadDataFromFile("schedule.txt",
                tokens => new Schedule(Convert.ToInt32(tokens[0]),
                                       Convert.ToInt32(tokens[1])));

            // "689155|Xavier|Quintas" => ["689155", "Xavier", "Quintas"]
            patients = EasyFile<Patient>.LoadDataFromFile("patients.txt",
                tokens => new Patient(Convert.ToInt32(tokens[0]),
                                      tokens[1],
                                      tokens[2]));

            // "689155|0|0" => ["689155", "0", "0"]
            appointments = EasyFile<Appointment>.LoadDataFromFile("appointments.txt",
                tokens => new Appointment(Convert.ToInt32(tokens[0]),
                                          Convert.ToInt32(tokens[1]),
                                          Convert.ToInt32(tokens[2])));
        }

        public List<PendingAppointment> GetPendingAppointments()
        {
            List<PendingAppointment> list = new List<PendingAppointment>();

            appointments.ForEach(
                a => list.Add(
                    new PendingAppointment(
                        patients.Find(p => p.Id == a.PatientId),
                        days.Find(d => d.Id == a.DayId),
                        times.Find(t => t.Id == a.TimeId)))); 

            return list;
        }

        public List<Day> GetDays() => new List<Day>(days);

        public List<Time> GetTimes() => new List<Time>(times);

        public bool HasPendingAppointment(int patientId) =>
            appointments.Exists(a => a.PatientId == patientId);

        public List<Schedule> GetAvailableSchedule() =>
            schedules.FindAll(s =>
                !appointments.Exists(a => 
                    a.DayId == s.DayId && a.TimeId == s.TimeId));

        public void AddAppointment(int patientId, int dayId, int timeId)
        {
            appointments.Add(new Appointment(patientId, dayId, timeId));

            EasyFile<Appointment>.SaveDataToFile(
                "appointments.txt",
                new string[] { "PatientId", "DayId", "TimeId" },
                appointments);
        }

        public void CancelAppointment(int patientId)
        {
            appointments.RemoveAll(a => a.PatientId == patientId);

            EasyFile<Appointment>.SaveDataToFile(
                "appointments.txt",
                new string[] { "PatientId", "DayId", "TimeId" },
                appointments);
        }

        // Lambda methods
        /*Day Callback(string[] tokens)
        {
            return new Day(Convert.ToInt32(tokens[0]),
                            tokens[1]);
        }*/
    }
}
