﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentalCare
{
    class Schedule
    {
        public int DayId { get; private set; }
        public int TimeId { get; private set; }

        public Schedule(int dayId, int timeId)
        {
            DayId = dayId;
            TimeId = timeId;
        }
    }
}
