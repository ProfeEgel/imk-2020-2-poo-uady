﻿
namespace FormsIntro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.GroupBox groupBox1;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            this.txtTest = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.chkMenta = new System.Windows.Forms.CheckBox();
            this.chkFresa = new System.Windows.Forms.CheckBox();
            this.chkChocochip = new System.Windows.Forms.CheckBox();
            this.chkLimon = new System.Windows.Forms.CheckBox();
            this.chkUva = new System.Windows.Forms.CheckBox();
            this.chkChicle = new System.Windows.Forms.CheckBox();
            this.txtFlavors = new System.Windows.Forms.TextBox();
            this.radMercurio = new System.Windows.Forms.RadioButton();
            this.radVenus = new System.Windows.Forms.RadioButton();
            this.radTierra = new System.Windows.Forms.RadioButton();
            this.radSaturno = new System.Windows.Forms.RadioButton();
            this.radJupiter = new System.Windows.Forms.RadioButton();
            this.radMarte = new System.Windows.Forms.RadioButton();
            this.radNeptuno = new System.Windows.Forms.RadioButton();
            this.radUrano = new System.Windows.Forms.RadioButton();
            this.txtPlanetas = new System.Windows.Forms.TextBox();
            this.nudX = new System.Windows.Forms.NumericUpDown();
            this.nudY = new System.Windows.Forms.NumericUpDown();
            this.txtSuma = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            groupBox1 = new System.Windows.Forms.GroupBox();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudY)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(50, 24);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(51, 24);
            label1.TabIndex = 0;
            label1.Text = "Test:";
            // 
            // txtTest
            // 
            this.txtTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTest.Location = new System.Drawing.Point(107, 21);
            this.txtTest.Name = "txtTest";
            this.txtTest.ReadOnly = true;
            this.txtTest.Size = new System.Drawing.Size(297, 29);
            this.txtTest.TabIndex = 1;
            // 
            // btnTest
            // 
            this.btnTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTest.Location = new System.Drawing.Point(410, 17);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(119, 38);
            this.btnTest.TabIndex = 2;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // chkMenta
            // 
            this.chkMenta.AutoSize = true;
            this.chkMenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMenta.Location = new System.Drawing.Point(107, 114);
            this.chkMenta.Name = "chkMenta";
            this.chkMenta.Size = new System.Drawing.Size(73, 24);
            this.chkMenta.TabIndex = 3;
            this.chkMenta.Text = "Menta";
            this.chkMenta.UseVisualStyleBackColor = true;
            this.chkMenta.CheckedChanged += new System.EventHandler(this.chkMenta_CheckedChanged);
            // 
            // chkFresa
            // 
            this.chkFresa.AutoSize = true;
            this.chkFresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFresa.Location = new System.Drawing.Point(107, 144);
            this.chkFresa.Name = "chkFresa";
            this.chkFresa.Size = new System.Drawing.Size(69, 24);
            this.chkFresa.TabIndex = 3;
            this.chkFresa.Text = "Fresa";
            this.chkFresa.UseVisualStyleBackColor = true;
            this.chkFresa.CheckedChanged += new System.EventHandler(this.chkMenta_CheckedChanged);
            // 
            // chkChocochip
            // 
            this.chkChocochip.AutoSize = true;
            this.chkChocochip.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkChocochip.Location = new System.Drawing.Point(107, 174);
            this.chkChocochip.Name = "chkChocochip";
            this.chkChocochip.Size = new System.Drawing.Size(103, 24);
            this.chkChocochip.TabIndex = 3;
            this.chkChocochip.Text = "Chocochip";
            this.chkChocochip.UseVisualStyleBackColor = true;
            this.chkChocochip.CheckedChanged += new System.EventHandler(this.chkMenta_CheckedChanged);
            // 
            // chkLimon
            // 
            this.chkLimon.AutoSize = true;
            this.chkLimon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLimon.Location = new System.Drawing.Point(255, 114);
            this.chkLimon.Name = "chkLimon";
            this.chkLimon.Size = new System.Drawing.Size(71, 24);
            this.chkLimon.TabIndex = 3;
            this.chkLimon.Text = "Limon";
            this.chkLimon.UseVisualStyleBackColor = true;
            this.chkLimon.CheckedChanged += new System.EventHandler(this.chkMenta_CheckedChanged);
            // 
            // chkUva
            // 
            this.chkUva.AutoSize = true;
            this.chkUva.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUva.Location = new System.Drawing.Point(255, 144);
            this.chkUva.Name = "chkUva";
            this.chkUva.Size = new System.Drawing.Size(56, 24);
            this.chkUva.TabIndex = 3;
            this.chkUva.Text = "Uva";
            this.chkUva.UseVisualStyleBackColor = true;
            this.chkUva.CheckedChanged += new System.EventHandler(this.chkMenta_CheckedChanged);
            // 
            // chkChicle
            // 
            this.chkChicle.AutoSize = true;
            this.chkChicle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkChicle.Location = new System.Drawing.Point(255, 174);
            this.chkChicle.Name = "chkChicle";
            this.chkChicle.Size = new System.Drawing.Size(71, 24);
            this.chkChicle.TabIndex = 3;
            this.chkChicle.Text = "Chicle";
            this.chkChicle.UseVisualStyleBackColor = true;
            this.chkChicle.CheckedChanged += new System.EventHandler(this.chkMenta_CheckedChanged);
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(25, 72);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(76, 24);
            label2.TabIndex = 0;
            label2.Text = "Flavors:";
            // 
            // txtFlavors
            // 
            this.txtFlavors.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFlavors.Location = new System.Drawing.Point(107, 69);
            this.txtFlavors.Name = "txtFlavors";
            this.txtFlavors.ReadOnly = true;
            this.txtFlavors.Size = new System.Drawing.Size(422, 29);
            this.txtFlavors.TabIndex = 1;
            // 
            // radMercurio
            // 
            this.radMercurio.AutoSize = true;
            this.radMercurio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radMercurio.Location = new System.Drawing.Point(23, 37);
            this.radMercurio.Name = "radMercurio";
            this.radMercurio.Size = new System.Drawing.Size(88, 24);
            this.radMercurio.TabIndex = 4;
            this.radMercurio.Text = "Mercurio";
            this.radMercurio.UseVisualStyleBackColor = true;
            this.radMercurio.CheckedChanged += new System.EventHandler(this.radTierra_CheckedChanged);
            // 
            // radVenus
            // 
            this.radVenus.AutoSize = true;
            this.radVenus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radVenus.Location = new System.Drawing.Point(23, 67);
            this.radVenus.Name = "radVenus";
            this.radVenus.Size = new System.Drawing.Size(73, 24);
            this.radVenus.TabIndex = 4;
            this.radVenus.Text = "Venus";
            this.radVenus.UseVisualStyleBackColor = true;
            this.radVenus.CheckedChanged += new System.EventHandler(this.radTierra_CheckedChanged);
            // 
            // radTierra
            // 
            this.radTierra.AutoSize = true;
            this.radTierra.Checked = true;
            this.radTierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTierra.Location = new System.Drawing.Point(23, 97);
            this.radTierra.Name = "radTierra";
            this.radTierra.Size = new System.Drawing.Size(67, 24);
            this.radTierra.TabIndex = 4;
            this.radTierra.TabStop = true;
            this.radTierra.Text = "Tierra";
            this.radTierra.UseVisualStyleBackColor = true;
            this.radTierra.CheckedChanged += new System.EventHandler(this.radTierra_CheckedChanged);
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(this.radUrano);
            groupBox1.Controls.Add(this.radNeptuno);
            groupBox1.Controls.Add(this.radMarte);
            groupBox1.Controls.Add(this.radJupiter);
            groupBox1.Controls.Add(this.radMercurio);
            groupBox1.Controls.Add(this.radSaturno);
            groupBox1.Controls.Add(this.radVenus);
            groupBox1.Controls.Add(this.radTierra);
            groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            groupBox1.Location = new System.Drawing.Point(107, 260);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new System.Drawing.Size(394, 151);
            groupBox1.TabIndex = 5;
            groupBox1.TabStop = false;
            groupBox1.Text = "Planetas";
            // 
            // radSaturno
            // 
            this.radSaturno.AutoSize = true;
            this.radSaturno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radSaturno.Location = new System.Drawing.Point(164, 97);
            this.radSaturno.Name = "radSaturno";
            this.radSaturno.Size = new System.Drawing.Size(84, 24);
            this.radSaturno.TabIndex = 4;
            this.radSaturno.Text = "Saturno";
            this.radSaturno.UseVisualStyleBackColor = true;
            this.radSaturno.CheckedChanged += new System.EventHandler(this.radTierra_CheckedChanged);
            // 
            // radJupiter
            // 
            this.radJupiter.AutoSize = true;
            this.radJupiter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radJupiter.Location = new System.Drawing.Point(164, 67);
            this.radJupiter.Name = "radJupiter";
            this.radJupiter.Size = new System.Drawing.Size(75, 24);
            this.radJupiter.TabIndex = 4;
            this.radJupiter.Text = "Júpiter";
            this.radJupiter.UseVisualStyleBackColor = true;
            this.radJupiter.CheckedChanged += new System.EventHandler(this.radTierra_CheckedChanged);
            // 
            // radMarte
            // 
            this.radMarte.AutoSize = true;
            this.radMarte.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radMarte.Location = new System.Drawing.Point(164, 37);
            this.radMarte.Name = "radMarte";
            this.radMarte.Size = new System.Drawing.Size(68, 24);
            this.radMarte.TabIndex = 4;
            this.radMarte.Text = "Marte";
            this.radMarte.UseVisualStyleBackColor = true;
            this.radMarte.CheckedChanged += new System.EventHandler(this.radTierra_CheckedChanged);
            // 
            // radNeptuno
            // 
            this.radNeptuno.AutoSize = true;
            this.radNeptuno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNeptuno.Location = new System.Drawing.Point(289, 67);
            this.radNeptuno.Name = "radNeptuno";
            this.radNeptuno.Size = new System.Drawing.Size(88, 24);
            this.radNeptuno.TabIndex = 4;
            this.radNeptuno.Text = "Neptuno";
            this.radNeptuno.UseVisualStyleBackColor = true;
            this.radNeptuno.CheckedChanged += new System.EventHandler(this.radTierra_CheckedChanged);
            // 
            // radUrano
            // 
            this.radUrano.AutoSize = true;
            this.radUrano.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radUrano.Location = new System.Drawing.Point(289, 37);
            this.radUrano.Name = "radUrano";
            this.radUrano.Size = new System.Drawing.Size(71, 24);
            this.radUrano.TabIndex = 4;
            this.radUrano.Text = "Urano";
            this.radUrano.UseVisualStyleBackColor = true;
            this.radUrano.CheckedChanged += new System.EventHandler(this.radTierra_CheckedChanged);
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(25, 217);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(210, 24);
            label3.TabIndex = 0;
            label3.Text = "¿En qué planeta vives?:";
            // 
            // txtPlanetas
            // 
            this.txtPlanetas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlanetas.Location = new System.Drawing.Point(241, 214);
            this.txtPlanetas.Name = "txtPlanetas";
            this.txtPlanetas.ReadOnly = true;
            this.txtPlanetas.Size = new System.Drawing.Size(288, 29);
            this.txtPlanetas.TabIndex = 1;
            this.txtPlanetas.Text = "Tierra";
            // 
            // nudX
            // 
            this.nudX.DecimalPlaces = 1;
            this.nudX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudX.Location = new System.Drawing.Point(680, 69);
            this.nudX.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudX.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudX.Name = "nudX";
            this.nudX.Size = new System.Drawing.Size(89, 26);
            this.nudX.TabIndex = 6;
            this.nudX.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudX.ValueChanged += new System.EventHandler(this.nudX_ValueChanged);
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(645, 68);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(29, 24);
            label4.TabIndex = 0;
            label4.Text = "X:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(645, 106);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(27, 24);
            label5.TabIndex = 0;
            label5.Text = "Y:";
            // 
            // nudY
            // 
            this.nudY.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudY.Location = new System.Drawing.Point(680, 107);
            this.nudY.Name = "nudY";
            this.nudY.Size = new System.Drawing.Size(89, 26);
            this.nudY.TabIndex = 6;
            this.nudY.ValueChanged += new System.EventHandler(this.nudX_ValueChanged);
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.Location = new System.Drawing.Point(612, 24);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(62, 24);
            label6.TabIndex = 0;
            label6.Text = "X + Y:";
            // 
            // txtSuma
            // 
            this.txtSuma.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSuma.Location = new System.Drawing.Point(680, 21);
            this.txtSuma.Name = "txtSuma";
            this.txtSuma.ReadOnly = true;
            this.txtSuma.Size = new System.Drawing.Size(141, 29);
            this.txtSuma.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(985, 423);
            this.Controls.Add(this.nudY);
            this.Controls.Add(this.nudX);
            this.Controls.Add(groupBox1);
            this.Controls.Add(this.chkChicle);
            this.Controls.Add(this.chkChocochip);
            this.Controls.Add(this.chkUva);
            this.Controls.Add(this.chkFresa);
            this.Controls.Add(this.chkLimon);
            this.Controls.Add(this.chkMenta);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.txtPlanetas);
            this.Controls.Add(label3);
            this.Controls.Add(this.txtFlavors);
            this.Controls.Add(label2);
            this.Controls.Add(label5);
            this.Controls.Add(this.txtSuma);
            this.Controls.Add(this.txtTest);
            this.Controls.Add(label6);
            this.Controls.Add(label4);
            this.Controls.Add(label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtTest;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.CheckBox chkMenta;
        private System.Windows.Forms.CheckBox chkFresa;
        private System.Windows.Forms.CheckBox chkChocochip;
        private System.Windows.Forms.CheckBox chkLimon;
        private System.Windows.Forms.CheckBox chkUva;
        private System.Windows.Forms.CheckBox chkChicle;
        private System.Windows.Forms.TextBox txtFlavors;
        private System.Windows.Forms.RadioButton radMercurio;
        private System.Windows.Forms.RadioButton radVenus;
        private System.Windows.Forms.RadioButton radTierra;
        private System.Windows.Forms.RadioButton radUrano;
        private System.Windows.Forms.RadioButton radNeptuno;
        private System.Windows.Forms.RadioButton radMarte;
        private System.Windows.Forms.RadioButton radJupiter;
        private System.Windows.Forms.RadioButton radSaturno;
        private System.Windows.Forms.TextBox txtPlanetas;
        private System.Windows.Forms.NumericUpDown nudX;
        private System.Windows.Forms.NumericUpDown nudY;
        private System.Windows.Forms.TextBox txtSuma;
    }
}

