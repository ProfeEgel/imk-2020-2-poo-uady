﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsIntro
{
    public partial class Form1 : Form
    {
        private int contador = 0;

        public Form1()
        {
            InitializeComponent();

            txtTest.Text = "Contador: 0";
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            //txtTest.Text = "¡Hola Windows Forms!";
            txtTest.Text = $"Contador: {++contador}";
        }

        private void chkMenta_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox[] chkFlavorsArray = { chkMenta, chkFresa, chkChocochip,
                                           chkLimon, chkUva, chkChicle };

            List<string> flavorsStrings = new List<string>();
            foreach (CheckBox chk in chkFlavorsArray)
            {
                if (chk.Checked)
                {
                    flavorsStrings.Add(chk.Text);
                }
            }

            txtFlavors.Text = String.Join(", ", flavorsStrings);

            //if (chkMenta.Checked)
            //{
            //    txtFlavors.Text = "Menta";
            //}
            //else
            //{
            //    txtFlavors.Clear();
            //}
        }

        private void radTierra_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton planeta = sender as RadioButton;
            if (planeta.Checked)
            {
                txtPlanetas.Text = planeta.Text;
            }
        }

        private void nudX_ValueChanged(object sender, EventArgs e)
        {
            txtSuma.Text = $"{nudX.Value + nudY.Value}";
        }
    }
}
