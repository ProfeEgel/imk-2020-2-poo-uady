﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.Console;

namespace FirstProject
{
    class Program
    {
        /*
         * Comentario estilo C
         */

        // Comentario estilo C++

        // Programa para resolver ax^2 + bx + c = 0
        // a=b=c=0, no es una ecuación cuadrática
        // a=b=0, no es una ecuación válida
        // a=0, es una ec. lineal, una solución x = -c/b
        // a!=0, es una ec. cuadrática, dos soluciones, fórmula general
        
        static void Main(string[] args)
        {
            // { ... } Bloque de código

            bool condition = false;
            do
            {
                Clear(); // system("cls")
                WriteLine("****************************************");
                WriteLine("Programa para resolver ax^2 + bx + c = 0");
                WriteLine("****************************************");
                
                WriteLine("\n=> Introduce el valor de las variables");
                Write("a = ");
                int a = Convert.ToInt32(ReadLine());
                Write("b = ");
                int b = Convert.ToInt32(ReadLine());
                Write("c = ");
                int c = Convert.ToInt32(ReadLine());

                if (a == 0 && b == 0 && c == 0)
                {
                    WriteLine("Error: no es una ecuación cuadrática...");
                }
                else if (a == 0 && b == 0)
                {
                    WriteLine("Error: no es una ecuación válida...");
                }
                else if (a == 0)
                {
                    WriteLine("\nInformación: es una ecuación lineal.");

                    double x = -c / (double)b;
                    WriteLine($"La solución es {x}");
                }
                else
                {
                    int D = b * b - 4 * a * c;

                    if (D > 0) // raices reales y diferentes
                    {
                        double x1 = (-b + Math.Sqrt(D)) / (2 * a);
                        double x2 = (-b - Math.Sqrt(D)) / (2 * a);

                        WriteLine("\nLas raíces son reales y diferentes:");
                        WriteLine($" x1 = {x1}");
                        WriteLine($" x2 = {x2}");
                    }
                    else if (D == 0) // raíces reales e iguales
                    {
                        double x = -b / (2.0 * a);

                        WriteLine("\nLas raíces son reales e iguales:");
                        WriteLine($" x1 = x2 = {x}");
                    }
                    else // raíces complejas conjugadas
                    {
                        double real = -b / (2.0 * a);
                        double imag = Math.Sqrt(-D) / (2 * a);

                        WriteLine("\nLas raíces son complejas conjugadas:");
                        WriteLine($" x1 = {real:F4} + {imag:F4} i");
                        WriteLine($" x2 = {real:F4} - {imag:F4} i");
                    }
                }

                Write("\n¿Deseas resolver otra ecuación? [s/n]:");
                condition = ReadLine().ToUpper()[0] == 'S';

            } while (condition);
        }

        /*
        static void Main(string[] args)
        {
            Console.WriteLine("¡Primer programa en C#!");
        }
        */
    }
}
